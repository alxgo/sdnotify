package sdnotify

import (
    "errors"
    "net"
    "os"
    "time"
)

var SdNotifyNoSocket = errors.New("No socket")

// Send a message to the init daemon. It is common to ignore the error.
func SdNotify(state string) error {
    socketAddr := &net.UnixAddr{
        Name: os.Getenv("NOTIFY_SOCKET"),
        Net:  "unixgram",
    }

    if socketAddr.Name == "" {
        return SdNotifyNoSocket
    }

    conn, err := net.DialUnix(socketAddr.Net, nil, socketAddr)
    if err != nil {
        return err
    }

    _, err = conn.Write([]byte(state))
    if err != nil {
        return err
    }

    return nil
}

func SdNotifyReady() error {
    return SdNotify("READY=1")
}

func SdNotifyStopping() error {
    return SdNotify("STOPPING=1")
}

func SdNotifyReloading() error {
    return SdNotify("RELOADING=1")
}

func SdNotifyStatus(status string) error {
    return SdNotify("STATUS=" + status)
}

func SdNotifyWatchdog() error {
    return SdNotify("WATCHDOG=1")
}

func NotifyWatchdog() {
    SdNotifyReady()

    for range time.Tick(time.Second *1) {
        SdNotifyWatchdog()
    }
}
